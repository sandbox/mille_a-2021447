<?php

/**
 * the core class of omodule_core
 */
abstract class ObjectModuleCore {

  /**
   * the key used in drupal variable and global variable
   * list of modules using ObjectModuleCore
   */
  static private $globalName = 'omodule_core_modules';

  /**
   * fast method to up to day the drupal variable
   */
  static private function SetDrupalVar(array $value) {
    $value = array_values($value);
    rsort($value, SORT_STRING);
    variable_set(ObjectModuleCore::$globalName, $value);
  }

  /**
   * Unset a specific module in the previous list
   */
  static private function UnsetModule($moduleName) {
    $conf = ObjectModuleCore::Installed(); // get the module list
    $key  = array_search($moduleName, $conf); // search the key to $moduleName
    if ($key != false) // unset if it is installed
      unset($conf[$key]);
    // up to day the drupal variable
    ObjectModuleCore::SetDrupalVar($conf);
  }

  /**
   * Don't call manually this method
   * 
   * this method is used to Init modules using ObjectModuleCore
   */
  static public function Init() {
    $modules = ObjectModuleCore::Installed(true); // get the module list
    foreach ($modules as $moduleInfo) { // work for each module
      if (!class_exists($moduleInfo->name)) { // if module not exist
        ObjectModuleCore::UnsetModule($moduleInfo->name); // uninstall it
        continue; // next module
      }
      if (!$moduleInfo->obj) { // if the module is not yet instantiated
        $class           = $moduleInfo->name;
        $moduleInfo->obj = new $class; // instantiate it
      }
      // add it in drupal variable
      ObjectModuleCore::set($moduleInfo->name, $moduleInfo->obj);
    }
  }

  /**
   * Return the list of modules using ObjectModuleCore
   * 
   * if $assoc is equal at true, the array associate the object instantiated
   */
  static public function Installed($assoc = false) {
    // get the module list
    $conf = variable_get(ObjectModuleCore::$globalName, array());
    // catch eventual error
    if (!is_array($conf))
      $conf = array();

    if (!$assoc) // if the assoc is not needed
      return $conf;
    // else find the object instantiated to each module
    foreach ($conf as $key => $module)
      $conf[$key] = (object) array(
            'name' => $module,
            'obj'  => ObjectModuleCore::get($module)
      );
    return $conf;
  }

  /**
   * Call this method in the module hook_install()
   * 
   * this method add a module using ObjectModuleCore in drupal variable
   * this method is also using to init drupal variable if $obj is equal at null
   */
  static public function Install($obj = null) {
    $conf = ObjectModuleCore::Installed(); // get the module list
    if ($obj) { // if this method is used to add a module
      $classInfo = new ReflectionClass($obj); // get class info
      if (!in_array($classInfo->name, $conf)) // not yet add
        $conf[]    = $classInfo->name; // add it
    }
    // set drupal variable
    ObjectModuleCore::SetDrupalVar($conf);
  }

  /**
   * Call this method to Uninstall a module using ObjectModuleCore
   * 
   * this module is also using to delete drupal variable
   * 
   * $obj can be an Object class (module class) or an string (module name)
   */
  static public function Uninstall($obj = null) {
    if ($obj) { // if this method is used to uninstall a module
      $classInfo = new ReflectionClass($obj); // get info class
      ObjectModuleCore::UnsetModule($classInfo->name); // uninstall it
      return; // stop job here
    }
    // delete drupal variable
    variable_del(ObjectModuleCore::$globalName);
  }

  /**
   * method to get an object instantiated using ObjectModuleCore
   */
  static private function get($key) {
    // check if this module ($key) is under control by OModule
    if (!in_array($key, ObjectModuleCore::Installed()))
      return null; // no so return null

      
// get global variable
    if (!isset($GLOBALS[ObjectModuleCore::$globalName]))
      $GLOBALS[ObjectModuleCore::$globalName] = array();

    // get it or return null
    $conf = &$GLOBALS[ObjectModuleCore::$globalName];
    return (!isset($conf[$key])) ? new $key : $conf[$key];
  }

  /**
   * Public alias to get a specific OModule
   */
  static public function GetOModule($module) {
    return ObjectModuleCore::get($module);
  }

  /**
   * method to add an object using ObjectModuleCore
   */
  static protected function set($key, $obj) {
    // get global variable
    if (!isset($GLOBALS[ObjectModuleCore::$globalName]))
      $GLOBALS[ObjectModuleCore::$globalName] = array();

    // add it
    $GLOBALS[ObjectModuleCore::$globalName][$key] = $obj;
  }

  /**
   * method to split function call and return original hook name and module name
   */
  static private function SplitHook($hook) {
    // init data returned
    $call         = new stdClass();
    $call->module = null;
    $call->hook   = $hook;

    // get list of modules using OModule
    $modules = ObjectModuleCore::Installed();
    foreach ($modules as $module)
      if (!strncmp($hook, $module, strlen($module))) {
        $call->module = $module;
        $call->hook   = substr_replace($hook, 'hook', 0, strlen($module));
      }

    return $call;
  }

  /**
   * hook_invoke() is call when a hook_* is invoked
   * 
   * hook_invoke allow to pass 7 parameters and reference if needed
   */
  static public function hook_invoke($hook, &$p1 = null, &$p2 = null, &$p3 = null, &$p4 = null, &$p5 = null, &$p6 = null, &$p7 = null) {
    /**
     * get the original hook name and the object associated
     */
    $call   = ObjectModuleCore::SplitHook($hook);
    $obj    = ObjectModuleCore::get($call->module);
    if ($obj == null) // if the system can't find the object, return null
      return null;
    $method = new ReflectionMethod($obj, $call->hook);

    // list arguments to pass
    $args   = array();
    $nargs  = func_num_args();
    for ($i = 1; $i < $nargs; $i++)
      $args[] = &${'p' . $i};

    // invoke hook method on the object module targeted
    return $method->invokeArgs($obj, $args);
  }

  /**
   *  this method is used to build prototype code
   * 
   *  if $writeRef is true, so the reference is included
   */
  static protected function BuildPrototype($methodInfo, $writeRef = true) {
    // get method list of parameters
    $args = $methodInfo->getParameters();
    foreach ($args as $i => &$argInfo) { // job for each
      $codeArg  = '$' . $argInfo->name;
      if ($argInfo->isPassedByReference() && $writeRef)
        $codeArg  = '&' . $codeArg; // add ref if asked
      $args[$i] = $codeArg; // add arg in array
    }
    return $args;
  }

}