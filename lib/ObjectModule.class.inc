<?php

/**
 * Load required class
 */
require_once __DIR__ . '/ObjectModuleCore.class.inc';

/**
 * this class must be implemented
 * 
 * it had the module object in ObjectModuleCore system
 */
abstract class ObjectModule extends ObjectModuleCore {

  private $_hooks = array();

  public function Hooks() {
    return $this->_hooks;
  }

  public function __construct() {
    /**
     * get info class and public methods list
     */
    $classInfo = new ReflectionClass($this);
    ObjectModuleCore::set($classInfo->name, $this);
    $methods   = $classInfo->getMethods(ReflectionMethod::IS_PUBLIC);
    foreach ($methods as &$methodInfo) { // job for each method
      // work for only hook method
      if (strncmp($methodInfo->name, 'hook_', 5))
        continue;

      // re-build hook name (with module name)
      $hook_name = $classInfo->name . '_';
      $hook_name.= substr($methodInfo->name, 5);
      $code      = 'function ' . $hook_name . '(';

      // get prototypes
      $prototype = ObjectModuleCore::BuildPrototype($methodInfo);
      $call      = ObjectModuleCore::BuildPrototype($methodInfo, false);
      array_unshift($call, '__FUNCTION__');

      // ensure hook
      $this->_hooks[] = (object) array(
            'name'       => $methodInfo->name,
            'parameters' => $prototype
      );

      // build function code
      $code.= implode(',', $prototype) . '){return ';
      $code.= 'ObjectModuleCore::hook_invoke(' . implode(',', $call) . ');';
      $code.= '}';

      // eval it
      eval($code);
    }
  }

}